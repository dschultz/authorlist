
collabs = {
    'icecube': 'IceCube Collaboration',
    'pingu': 'IceCube/PINGU Collaboration',
    'icecube-gen2': 'IceCube-Gen2 Collaboration',
}

ICECUBE_START_DATE = '2003-01-01'
PINGU_START_DATE = '2013-06-25'
GEN2_START_DATE = '2014-12-16'
